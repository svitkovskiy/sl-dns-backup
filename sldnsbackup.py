#!/usr/bin/env python
import sys
import os
import json
from optparse import OptionParser
from ConfigParser import SafeConfigParser

from SoftLayer import Client, API_PRIVATE_ENDPOINT, API_PUBLIC_ENDPOINT

CONFIG_SECTION="Main"
VERSION = '1.1'


def filter_domain(account_svc, domains_filter=None):
    """Matches all account's domain to the list if it's present. If
    domains_filter is empty or None, all domains are returned"""
    all_domains = account_svc.getDomains()
    if not domains_filter:
        return all_domains
    result = []
    domains_filter = map(lambda x: x.lower(), domains_filter)
    for domain_obj in all_domains:
        if domain_obj['name'].lower() in domains_filter:
            result.append(domain_obj)
    return result


def open_file(domain_obj, options):
    if options.stdout:
        return sys.stdout

    filename = os.path.expanduser(os.path.join(options.base,
        options.template).format(zone=domain_obj['name'].lower(),
        serial=domain_obj['serial'], id=domain_obj['id'],
        timestamp=domain_obj.get('updateDate','Unknown')))
    dirname = os.path.dirname(filename)
    if not os.path.exists(dirname):
        os.makedirs(dirname, mode=0700)
    if options.overwrite is False and os.path.exists(filename):
        return False
    return open(filename, 'w')


def format_rr(domain_obj, r):
    rr_type = r['type'].upper()

    if rr_type == 'SOA':
        result = "@\t%i\tIN SOA %s %s (\n"%(r['ttl'], r['data'],
            r['responsiblePerson'].lower())
        result += "\t\t\t%s\t; serial\n"%domain_obj['serial']
        result += "\t\t\t%s\t\t; slave refresh\n"%r['refresh']
        result += "\t\t\t%s\t\t; slave retry\n"%r['retry']
        result += "\t\t\t%s\t\t; slave expiration\n"%r['expire']
        result += "\t\t\t%s)\t\t; max retry time\n"%r['minimum']
        return result

    if rr_type == 'MX':
        return "%s\t%i\tIN MX %i %s\n"%(r['host'], r['ttl'], r['mxPriority'],
            r['data'])
    return "%s\t%i\tIN %s %s\n"%(r['host'], r['ttl'], rr_type, r['data'])


def write_domain(client, domain_obj, options, f):
    domain_name = domain_obj.get('name').lower()
    f.write("; Last updated: %s\n"%domain_obj.get('updateDate','Unknown'))
    f.write("$ORIGIN %s.\n"%domain_name)
    json_dump = "\n;D:%s\n"%json.dumps(domain_obj)

    for rr in client['SoftLayer_Dns_Domain'].getResourceRecords(id=domain_obj['id']):
        f.write(format_rr(domain_obj, rr))
        json_dump += ";R:%s\n"%json.dumps(rr)

    if options.json:
        f.write(json_dump)
    f.close()


def parse_options():
    parser = OptionParser(usage="usage: %prog [options] [domain1 [domain2 [...]]]",
            version="%%prog %s"%VERSION)
    parser.add_option("-d", "--base", dest="base",
            default="~/.sl-dns-backup",
            help="All backups will go to {base}/{template} (%default)")
    parser.add_option("-t", "--template", dest="template",
            default="{zone}/{zone}-{serial}.zone",
            help="Backup file name template. Fields available: {zone}, {serial}, {id}, {timestamp}. (%default)")
    parser.add_option("-j", "--json", dest="json", action="store_true",
            default=False,
            help="Save commented out JSON to zone files. Preserves SL Object IDs")
    parser.add_option("-s", "--stdout", dest="stdout", action="store_true",
            default=False, help="Dump zone(s) to stdout")
    parser.add_option("-o", "--overwrite", dest="overwrite", action="store_true",
            default=False, help="Overwrite file if it exists")
    parser.add_option("-c", "--config", dest="config_file",
            default="~/.sl-dns-backup.ini", help="Config file (%default)")
    parser.add_option("-u", "--username", dest="username",
            default=os.getenv("SL_API_USERNAME", None),
            help="SoftLayer API username (can use SL_API_USERNAME environment variable")
    parser.add_option("-k", "--key", dest="key",
            default=os.getenv("SL_API_KEY", None),
            help="SoftLayer API key (can use SL_API_KEY environment variable")
    parser.add_option("-p", "--private", dest="private", action="store_true",
            default=False, help="""Use private API endpoint - more secure but
            requires access to internal SoftLayer VLAN or VPN""")

    # Parsing command line options and arguments (domains to dump)
    (options, arguments) = parser.parse_args()

    # Parsing config (if any) and override config options
    config = SafeConfigParser()
    config.read(os.path.expanduser(options.config_file))
    if config.has_section(CONFIG_SECTION):
        new_defaults = {}
        for config_item in config.options(CONFIG_SECTION):
            if hasattr(options, config_item):
                if isinstance(getattr(options, config_item), bool):
                    new_defaults[config_item] = config.getboolean(CONFIG_SECTION, config_item)
                else:
                    new_defaults[config_item] = config.get(CONFIG_SECTION, config_item)
        if new_defaults:
            # Reparse options in case we have new defaults from the config file
            parser.set_defaults(**new_defaults)
            (options, arguments) = parser.parse_args()
    if not (options.username and options.key):
        parser.error("Please supply API username and key")
    return (options, arguments)


def main():
    (options, domains_to_backup) = parse_options()

    endpoint = API_PRIVATE_ENDPOINT if options.private else API_PUBLIC_ENDPOINT

    client = Client(username=options.username, api_key=options.key,
            endpoint_url=endpoint)
    account_svc = client['Account']

    for domain_obj in filter_domain(account_svc, domains_to_backup):
        f = open_file(domain_obj, options)
        if not f:
            continue
        write_domain(client, domain_obj, options, f)


if __name__ == '__main__':
    main()
