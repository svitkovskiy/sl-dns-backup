#!/usr/bin/env python
import os
import shutil

from setuptools import setup

setup(
        name = 'sl_dns_backup',
        version = '1.1',
        install_requires = ['SoftLayer==2.0.0', ],
        entry_points = {
            'console_scripts': [ 'sl-dns-backup = sldnsbackup:main', ],
            },
        py_modules = ['sldnsbackup', ],
        author = "Stanislav Vitkovskiy",
        description = "Makes backups of DNS zones from SoftLayer DNS service",
        keywords = "softlayer dns backup",
)
