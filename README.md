# SoftLayer DNS backup tool

## Purpose

The tool makes backup copies of zones, hosted at [SoftLayer](http://www.softlayer.com/) DNS services that are accessible via [API](http://sldn.softlayer.com/article/SoftLayer-API-Overview)

The zones are saved in [ISC Bind zone file format](http://en.wikipedia.org/wiki/Zone_file) with optional JSON representation.


## Requirements

* [Softlayer Python API client](https://github.com/softlayer/softlayer-api-python-client) (`pip install SoftLayer==2.0.0`)

## Running

    $ sl-dns-backup --help
    Usage: sl-dns-backup [options] [domain1 [domain2 [...]]]

    Options:
      --version             show program's version number and exit
      -h, --help            show this help message and exit
      -d BASE, --base=BASE  All backups will go to {base}/{template} (~/.sl-dns-
                            backup)
      -t TEMPLATE, --template=TEMPLATE
                            Backup file name template. Fields available: {zone},
                            {serial}, {id}, {timestamp}.
                            ({zone}/{zone}-{serial}.zone)
      -j, --json            Save commented out JSON to zone files. Preserves SL
                            Object IDs
      -s, --stdout          Dump zone(s) to stdout
      -o, --overwrite       Overwrite file if it exists
      -c CONFIG_FILE, --config=CONFIG_FILE
                            Config file (~/.sl-dns-backup.ini)
      -u USERNAME, --username=USERNAME
                            SoftLayer API username (can use SL_API_USERNAME
                            environment variable
      -k KEY, --key=KEY     SoftLayer API key (can use SL_API_KEY environment
                            variable
      -p, --private         Use private API endpoint - more secure but
                            requires access to internal SoftLayer VLAN or VPN


## Configuring

All options' defaults can be specified in the configureation file, by default
`~/.sl-dns-backup.ini`:

    [Main]
    username = ABCD-EFG
    key = XYZ
    base = /var/backups/sl-dns
    template = {zone}-{timestamp}.txt
    json = yes
    private = yes
